<!-- 
Please select the correct template above and fill it out below. 
These HTML comments will not be rendered so there's no need to delete them. 
Do *not* close any issues yourself, we will close things once done/handled accordingly.
For checklists put an x inside the [ ] like this: [x] to mark the checkbox.
The actions at the end of this template will be done automatically once submitted.
-->

### New Feature

## What does this MR do?

<!-- Briefly describe what this MR is about -->

## Related issues

<!-- Link related issues below.  i.e. Resolves #1234 -->

## Checklist

<!-- Put an x inside the [ ] like this: [x] to mark the checkbox. -->

- [ ] Application works as expected when running from source
- [ ] There are dependency changes and:
    - [ ] AppImage build compiles and runs successfully
    - [ ] MR has also been made to the Flatpak repo: [Coolero Flatpak](https://github.com/flathub/org.coolero.Coolero)
      with needed changes
- [ ] Mock tests are successful
- [ ] Performance considerations have been taken into account
- [ ] This MR is ready to be merged

/assign me

/assign_reviewer @codifryed

/label ~"type::Feature"
