[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-blue.svg?logo=gnu)](https://opensource.org/licenses/)
[![Gitlab pipeline status](https://gitlab.com/codifryed/coolero/badges/main/pipeline.svg)](https://gitlab.com/codifryed/coolero/-/commits/main)
[![GitLab Release (latest by SemVer)](https://img.shields.io/gitlab/v/release/30707566?sort=semver&logo=gitlab)](https://gitlab.com/codifryed/coolero/pipelines)
[![Discord](https://img.shields.io/badge/_-online-_?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/MbcgUFAfhV)
[![Linux](https://img.shields.io/badge/_-linux-blue?logo=linux&logoColor=fff)]()
[![Python](https://img.shields.io/badge/_-python-blue?logo=python&logoColor=fff)]()

# Coolero

is a program to monitor and control your cooling devices.  
It offers an easy-to-use user interface with various control features and also provides live thermal performance
details.

It is a frontend and enhancement of libraries like
[liquidctl](https://github.com/liquidctl/liquidctl) and others with a focus on cooling device control under Linux.  
Written in [Python](https://www.python.org/) it uses [PySide](https://wiki.qt.io/Qt_for_Python) for the UI
and [Poetry](https://python-poetry.org/) for dependency management.

This project is currently in active development and slowly working it's way towards it's first major release.

## Features

- System Overview Graph - choose what to focus on and see the effects of your configuration changes live and over time.
- Supports multiple devices and multiple versions of the same device.
- Internal profile scheduling - create speed profiles based on CPU, GPU or other device temperature sensors that aren't
  natively supported by the devices themselves.
- Last set profiles are saved and applied at startup
- A modern custom UI
- Supports most of the devices [liquidctl supports](https://github.com/liquidctl/liquidctl#supported-devices)
- _In progress:_ Other integrations to be able to control additional cooling devices

## Demo

![Demo](screenshots/coolero-demo.gif)

## Current Supported Devices:

Some devices are only partially supported or considered experimental,
see [liquidctl](https://github.com/liquidctl/liquidctl#supported-devices) for more specifics.

| Name                                            | Notes                                   |
|-------------------------------------------------|-----------------------------------------|
| NZXT Kraken Z (Z53, Z63 or Z73)                 | <sup>LCD Screen not yet supported</sup> |
| NZXT Kraken X (X53, X63 or X73)                 |                                         |
| NZXT Kraken X (X42, X52, X62 and X72)           |                                         |
| NZXT Kraken X31, X41, X61                       |                                         |
| NZXT Kraken X40, X60                            | <sup>experimental</sup>                 |
| NZXT Kraken M22                                 | <sup>lighting only device</sup>         |
| NZXT HUE 2, HUE 2 Ambient                       | <sup>lighting only device</sup>         |
| NZXT Smart Device V2                            |                                         |
| NZXT RGB & Fan Controller                       |                                         |
| NZXT Smart Device                               |                                         |
| NZXT Grid+ V3                                   |                                         |
| NZXT E500, E650, E850                           | <sup>PSU, partial support</sup>         |
| Corsair Hydro GT/GTX H80i, H100i, H110i         | <sup>experimental</sup>                 |
| Corsair Hydro v2 H80i, H100i, H115i             |                                         |
| Corsair Hydro Pro H100i, H115i, H150i           | <sup>partial support</sup>              |
| Corsair Hydro Platinum H100i, H100i SE, H115i   | <sup>partial support</sup>              |
| Corsair Hydro Pro XT H60i, H100i, H115i, H150i  | <sup>partial support</sup>              |
| Corsair iCUE Elite Capellix H100i, H115i, H150i | <sup>experimental</sup>                 |
| Corsair Commander Pro                           |                                         |
| Corsair Commander Core                          | <sup>experimental</sup>                 |
| Corsair Obsidian 1000D                          |                                         |
| Corsair Lighting Node Core, Pro                 | <sup>lighting only device</sup>         |
| Corsair HX750i, HX850i, HX1000i, HX1200i        | <sup>PSU</sup>                          |
| Corsair RM650i, RM750i, RM850i, RM1000i         | <sup>PSU</sup>                          |
| EVGA CLC 120 (CL12), 240, 280, 360              |                                         |
| Gigabyte RGB Fusion 2.0                         | <sup>lighting only device</sup>         |

## Installation

Installation is currently supported by AppImage, Flatpak, the AUR and from Source

### AppImage:

[![AppImageDownload](screenshots/download-appimage-banner.svg)](https://gitlab.com/api/v4/projects/30707566/packages/generic/appimage/latest/Coolero-x86_64.AppImage)  
Use the above link or goto the [Releases](https://gitlab.com/codifryed/coolero/-/releases) page and download a specific
version.  
The AppImage contains all the needed dependencies. Just make it executable and run it:

```bash
chmod +x Coolero-x86_64.AppImage
./Coolero-x86_64.AppImage
```

It's recommended to turn on **Check for updates** in Settings, which is disabled by default. Coolero will then ask if
you want to update it automatically if a newer version is available.

<details>
<summary>Click for more info about AppImages</summary>

<a href="https://appimage.org/">AppImage Website</a><br>

For improved desktop integration:
<ul>
    <li><a href="https://github.com/TheAssassin/AppImageLauncher">AppImageLauncher</a></li>
    <li><a href="https://github.com/probonopd/go-appimage/blob/master/src/appimaged/README.md">appimaged</a></li>
</ul>
</details>

### Flatpak:

You can checkout the [Coolero page on Flathub](https://flathub.org/apps/details/org.coolero.Coolero)

or install from the command line:

```commandline
flatpak install org.coolero.Coolero
```

### Source:

<details>
<summary>Click to view</summary>

#### Requirements:

* Linux
* [Python 3.10](https://www.python.org/)
    * including the python3.10-dev package (may already be installed)
* *Python 3.9 also works, but 3.10 is what is officially used.

#### System packages:

* Ubuntu:
    ```bash
    sudo apt install libusb-1.0-0 curl python3-virtualenv python3.10-venv build-essential libgl1-mesa-dev
    ```
* Fedora:
    ```bash
    sudo dnf install libusbx curl python3-virtualenv mesa-libGL-devel && sudo dnf groupinstall "C Development Tools and Libraries"
    ```
* More specifically:
    * LibUSB 1.0 (libusb-1.0, libusb-1.0-0, or libusbx from your system package manager)
    * curl
    * python3-virtualenv  (or python3.10-virtualenv)
    * python3-venv  (or python3.10-venv)
    * Packages needed to build Qt applications:
        * build-essential
        * libgl1-mesa-dev

#### [Poetry](https://python-poetry.org/) -

* install:
    ```bash
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python3 -
    ```
* run: `poetry --version` to make sure poetry works
* if needed, add `$HOME/.local/bin` to your PATH to execute poetry easily:
    ```bash
    export PATH=$HOME/.local/bin:$PATH
    ```
* if Python 3.10 is not your default python installation, then run the following in the project directory to give poetry
  access:
    ```bash
    poetry env use python3.10
    ```

#### Coolero:

* Clone the Repo:
    ```bash
    git clone git@gitlab.com:codifryed/coolero.git
    ```
* Install the dependencies from the newly created repo directory:
    ```bash
    poetry install
    ```
* run it:
    ```bash
    poetry run coolero
    ```

</details>

## Usage hints:

- Scroll or right click on the system overview to zoom the time frame
- clicking anywhere in the control graphs will apply the current settings. Changing any setting will apply it
  immediately.
- Check the settings page for some QoL options.

## Debugging

To help diagnose issues enabling debug output is invaluable. It will produce quite a bit of output from the different
internal systems to help determine what the cause might be. Output is sent to the command line (stdout) and to a
rotating log file under /tmp/coolero for convienence. Simply add the `--debug` option.

#### AppImage:

`./Coolero-x86_64.AppImage --debug`

#### Flatpak:

`flatpak run org.coolero.Coolero --debug`

#### From Source:

`poetry run coolero --debug`

## Acknowledgements

* Major thanks is owed to the python API of [liquidctl](https://github.com/liquidctl/liquidctl)
* A big inspiration is [GKraken](https://gitlab.com/leinardi/gkraken) written by Roberto Leinardi.
* UI based on [PyOneDark](https://github.com/Wanderson-Magalhaes/PyOneDark_Qt_Widgets_Modern_GUI) by Wanderson M.Pimenta

## License

This program is licensed under [GPLv3](COPYING.txt)  
also see [the copyright notice](COPYRIGHT.md)

## FAQ

- Should I use Liquid or CPU as a temperature source to control my pump/fans?
    - Quick answer: Liquid
    - The thermodynamics of liquid cooling are very different compared to the traditional method. Choose what works best
      for your situation.
- My UDev rules are messed up, how do I apply them again?
    - run Coolero from the command line with `--add-udev-rules` to have them re-applied
- I have an issue with X, what do I do?
    - Please join the discord channel if it's something small, otherwise opening an Issue ticket in GitLab is the best
      way to get something fixed.
- How do I get Coolero to start automatically when I start my computer?
    - Each distro has their own way to do this, from a simple menu option 'Startup Applications' to writing your own
      script
- Why should I use this GUI when I could do what Coolero does with a shell script?
    - Oh, you definitely can, and I would encourage you to do so. Coolero started out as a dynamic replacement for some
      of my own scripts with the added advantage of being able to visualize the data I was collecting. Until you do,
      there's this GUI.